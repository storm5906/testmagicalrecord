//
//  MainViewController.h
//  testMagicalRecord
//
//  Created by Bao Nguyen on 5/27/14.
//  Copyright (c) 2014 storm5906. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"
#import "EditNoteViewController.h"

@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
