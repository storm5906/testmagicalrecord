//
//  Note.m
//  testMagicalRecord
//
//  Created by Bao Nguyen on 5/27/14.
//  Copyright (c) 2014 storm5906. All rights reserved.
//

#import "Note.h"


@implementation Note

@dynamic title;
@dynamic keywords;
@dynamic body;
@dynamic date;

@end
