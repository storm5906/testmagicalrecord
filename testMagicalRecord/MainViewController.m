//
//  MainViewController.m
//  testMagicalRecord
//
//  Created by Bao Nguyen on 5/27/14.
//  Copyright (c) 2014 storm5906. All rights reserved.
//

#import "MainViewController.h"

// you can define MR_SHORTHAND to remove the "MR_" before each MagicalRecord method
// #define MR_SHORTHAND

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *notes;

@end


@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchNotes];
    [self.tableView reloadData];
}


- (void)setupView
{
    // create edit button
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editNotes:)];
    self.navigationItem.leftBarButtonItem = editButton;
    
    // create add button
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(addNote:)];
    self.navigationItem.rightBarButtonItem = addButton;

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // fetch notes
    [self fetchNotes];
}


- (void)fetchNotes
{
    // fetch notes using MagicalRecord
    self.notes = [NSMutableArray arrayWithArray:[Note MR_findAll]];
}


- (void)editNotes:(id)sender
{
    [self.tableView setEditing:!self.tableView.editing animated:YES];
}


- (void)addNote:(id)sender
{
    EditNoteViewController *editViewController = [[EditNoteViewController alloc] init];
//    EditNoteViewController *editViewController = [[EditNoteViewController alloc] initWithNibName:@"EditNoteViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:editViewController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"NoteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        // configure cell
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    Note *note = [self.notes objectAtIndex:indexPath.row];
    [cell.textLabel setText:note.title];
    [cell.detailTextLabel setText:note.keywords];
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Note *note = [self.notes objectAtIndex:indexPath.row];
        [self.notes removeObjectAtIndex:indexPath.row];
        // delete a Note entity
        [note MR_deleteEntity];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        // save data
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    EditNoteViewController *editViewController = [[EditNoteViewController alloc] initWithNote:[self.notes objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:editViewController animated:YES];
}

@end
