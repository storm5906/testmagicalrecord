//
//  EditNoteViewController.m
//  testMagicalRecord
//
//  Created by Bao Nguyen on 5/27/14.
//  Copyright (c) 2014 storm5906. All rights reserved.
//

#import "EditNoteViewController.h"

@interface EditNoteViewController ()

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *keywordsTextField;
@property (weak, nonatomic) IBOutlet UITextView *bodyTextView;

@property (strong, nonatomic) Note *note;

@end


@implementation EditNoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}


- (id)initWithNote:(Note *)note
{
    self = [super init];
    if (self)
    {
        self.note = note;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupView
{
    self.bodyTextView.layer.borderWidth = 0.5;
    self.bodyTextView.layer.borderColor = [UIColor blackColor].CGColor;
    
    // create save button
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save:)];
    [self.navigationItem setRightBarButtonItem:saveButton];
    
    // create cancel button
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    [self.navigationItem setLeftBarButtonItem:cancelButton];
   
    if (self.note)
    {
        // show data in note
        [self.titleTextField setText:self.note.title];
        [self.keywordsTextField setText:self.note.keywords];
        [self.bodyTextView setText:self.note.body];
    }
}


- (void)cancel:(id)sender
{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)save:(id)sender
{
    if (!self.note)
    {
        // create new Note entity
        self.note = [Note MR_createEntity];
        [self.note setDate:[NSDate date]];
    }
    
    [self.note setTitle:self.titleTextField.text];
    [self.note setKeywords:self.keywordsTextField.text];
    [self.note setBody:self.bodyTextView.text];
    
    // save data
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
