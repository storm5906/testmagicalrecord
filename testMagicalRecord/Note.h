//
//  Note.h
//  testMagicalRecord
//
//  Created by Bao Nguyen on 5/27/14.
//  Copyright (c) 2014 storm5906. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Note : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * keywords;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSDate * date;

@end
